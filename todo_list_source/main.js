// массив записей
var data = localStorage.getItem("todoList")
    ? JSON.parse(localStorage.getItem("todoList"))
    : [];

var doneTasks = localStorage.getItem("doneList")
    ? JSON.parse(localStorage.getItem("doneList"))
    : [];

// синхронизация localStorage и Массива JS
function storageSync() {
    localStorage.setItem("todoList", JSON.stringify(data));
    localStorage.setItem("doneList", JSON.stringify(doneTasks));
}

// рисуем начальный список
renderTodoList();
renderDoneList();

// Добавить новый элемент в список
document.getElementById("btn-create").addEventListener("click", function() {
    var value = document.getElementById("input-create").value;
    if (value) {
        addItemToDOM(value);
        document.getElementById("input-create").value = "";
        data.push(value);
        storageSync();
    }
});

// Добавление задачи по нажатию ENTER
document.getElementById("input-create").addEventListener("keyup", (event) => {
    if(event.keyCode == 13) {
        event.preventDefault();
        var value = document.getElementById("input-create").value;
        if (value) {
            addItemToDOM(value);
            document.getElementById("input-create").value = "";
            data.push(value);
            storageSync();
        }
    };
});

var search = document.getElementById("input-search");
search.addEventListener("keyup", () => {
    var value = search.value;
    if (value) {
        doneTasks.map((str) => {
            if (str.includes(value)) {
                element = document.getElementById(str).parentNode.parentNode;
                while(element != element.parentNode.firstChild)
                    swapElements(element, element.previousSibling);
            }
        });
    }
});

function swapElements(element1, element2) {
    var temp = document.createElement("div");
    element1.parentNode.insertBefore(temp, element1);

    // move element1 to right before element2
    element2.parentNode.insertBefore(element1, element2);

    // move element2 to right before where element1 used to be
    temp.parentNode.insertBefore(element2, temp);

    // remove temporary marker node
    temp.parentNode.removeChild(temp);
}

function renderTodoList() {
    if (!data.length) return;

    for (var i = 0; i < data.length; i++) {
        var value = data[i];
        addItemToDOM(value);
    }
}

function renderDoneList() {
    if (!doneTasks.length) return;

    for (var i = 0; i < doneTasks.length; i++) {
        var value = doneTasks[i];
        addItemToDoneList(value);
    }
}

function addItemToDOM(text) {
    var list = document.getElementById("todo-list");

    var item = document.createElement("li");
    item.classList = "collection-item";
    var listItemView = `
    <div class="item">
        <span class="item-text">${text}</span>
        <span class="secondary-content">
            <div class="item-btn item-btn-del btn-floating btn-small waves-effect waves-light red">x</div>
        </span>
    </div>`;
    item.innerHTML = listItemView;

    // добавим слушатель для удаления
    var buttonDelete = item.getElementsByClassName("item-btn-del")[0];
    buttonDelete.addEventListener("click", removeItem);
    list.appendChild(item);
}

function addItemToDoneList(text) {
    var list = document.getElementById("done-list");

    var item = document.createElement("li");
    item.classList = "collection-item";
    var listItemView = `
    <div class="item">
        <span class="item-text" id="${text}">${text}</span>
    </div>`;
    item.innerHTML = listItemView;
    list.appendChild(item);
}

function removeItem(e) {
    var item = this.parentNode.parentNode.parentNode;
    var list = item.parentNode;
    var value = item.getElementsByClassName("item-text")[0].innerText;

    doneTasks.push(data.splice(data.indexOf(value), 1)[0]);
    addItemToDoneList(value);
    list.removeChild(item);
    storageSync();
}
