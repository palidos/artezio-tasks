function matrixTurn1(matrix) {
    matrix = matrix.reverse();
    return Object.keys(matrix[0]).map(col => {
        return matrix.map(row => {
            return row[col];
        });
    });
}

function matrixTurn2(matrix) {
    matrix = matrix.reverse();
    for (var i = 0; i < matrix.length; i++) {
        for (var j = 0; j < i; j++) {
            var temp = matrix[i][j];
            matrix[i][j] = matrix[j][i];
            matrix[j][i] = temp;
        }
    }
    return matrix;
}
