describe("matrixTurn", function() {
    describe("Rotates the matrix 90 degrees", function() {
        it("\"Map\" realization", function() {
            assert.deepEqual(matrixTurn1([
                [1,  2,  3,  4],
                [5,  6,  7,  8],
                [9,  10, 11, 12],
                [13, 14, 15, 16]
            ]), [
                [ 13, 9, 5, 1 ],
                [ 14, 10, 6, 2 ],
                [ 15, 11, 7, 3 ],
                [ 16, 12, 8, 4 ]
            ]);
        });

        it("\"For\" realization", function() {
            assert.deepEqual(matrixTurn2([
                [1,  2,  3,  4],
                [5,  6,  7,  8],
                [9,  10, 11, 12],
                [13, 14, 15, 16]
            ]), [
                [ 13, 9, 5, 1 ],
                [ 14, 10, 6, 2 ],
                [ 15, 11, 7, 3 ],
                [ 16, 12, 8, 4 ]
            ]);
        });
    });
});
